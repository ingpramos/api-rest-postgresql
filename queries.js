var promise = require('bluebird');
var options = {
	promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectString = 'postgres://postgres:123456@localhost:5432/test';
var db = pgp(connectString);

// TASKS

function getList(req, res, next) {
	db.any('SELECT * FROM customers ORDER BY id ASC')
		.then(function(data) {
			res.status(200).json({
				status: 'success',
				data: data,
				message: 'retrieved list'
			});
		})
		.catch(function(err) {
			return next(err);
		});
}

function getOne(req, res, next) {
	db.any('SELECT * FROM customers WHERE id=' + req.params.id + '')
		.then(function(data) {
			res.status(200).json({
				status: 'success',
				data: data,
				message: 'retrieved list'
			});
		})
		.catch(function(err) {
			return next(err);
		});
}
function createOne(req, res, next) {
	db.none('INSERT INTO customers(firstname, lastname, age) VALUES (${firstname}, ${lastname}, ${age})', req.body)
		.then(function() {
			res.status(200).json({
				status: 'success',
				message: 'add success a customer'
			});
		})
		.catch(function(err) {
			return next(err);
		});
}
function editOne(req, res, next) {
	db.none('UPDATE customers SET firstname=${firstname}, lastname = ${lastname}, age=${age} WHERE id=' +
				req.params.id +
				'',
			req.body
		)
		.then(function() {
			res.status(200).json({
				status: 'success',
				message: 'update success a customer'
			});
		})
		.catch(function(err) {
			return next(err);
		});
}
function deleteOne(req, res, next) {
	db.none('DELETE FROM customers WHERE id=' + req.params.id + '', req.body)
		.then(function() {
			res.status(200).json({
				status: 'success',
				message: 'deleted success a customer'
			});
		})
		.catch(function(err) {
			return next(err);
		});
}

// SIGN IN

function createUser(req, res, next) {
	db.none('INSERT INTO users(username, email, password) VALUES (${username},${email},${password})', req.body)
		.then(function() {
			res.status(200).json({
				status: 'success',
				message: 'add succes an user'
			});
		})
		.catch(function(err) {
			return next(err);
		});
}

function editUser(req, res, next) {
	db.none('UPDATE users SET username=${username}, email = ${email}, password=${password} WHERE id=' +
				req.params.id +
				'',
			req.body
		)
		.then(function() {
			res.status(200).json({
				status: 'success',
				message: 'update success an user'
			});
		})
		.catch(function(err) {
			
			return next(err);
		});
}

// LOGIN

function getUsers(req, res, next) {
	db.any('SELECT * FROM users ORDER BY id ASC')
		.then(function(data) {
			res.status(200).json({
				status: 'success',
				data: data,
				message: 'retrieved users'
			});
		})
		.catch(function(err) {
			return next(err);
		});
}

module.exports = {
	getList: getList,
	getOne: getOne,
	createOne: createOne,
	editOne: editOne,
	deleteOne: deleteOne,
	createUser: createUser,
	editUser: editUser,
	getUsers: getUsers
};
