var express = require('express');
var router = express.Router();
var db = require('../queries');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Crud with PostgreSQL' });
});

// TASKS

router.get('/api/tasks', db.getList);
router.get('/api/tasks/:id', db.getOne);
router.post('/api/tasks', db.createOne);
router.put('/api/tasks/:id', db.editOne);
router.delete('/api/tasks/:id', db.deleteOne);

// SIGN IN

router.post('/api/users', db.createUser);
router.put('/api/users/:id', db.editUser);

// LOGIN

router.get('/api/users', db.getUsers);

module.exports = router;
